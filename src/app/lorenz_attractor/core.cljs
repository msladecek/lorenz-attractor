(ns lorenz-attractor.core
  (:require
   [quil.core :as q :include-macros true]
   [quil.middleware :as m]))

(def palettes
  {:nord
   {:background-colors
    {:light [216 222 233]
     :dark [46 52 64]}
    :colors
    [[191 97 106]
     [163 190 140]
     [235 203 139]
     [129 161 193]
     [180 142 173]
     [136 192 208]]}
   :gruvbox
   {:background-colors
    {:light [249 245 215]
     :dark [29 32 33]}
    :colors
    [[204 36 29]
     [152 151 26]
     [215 153 33]
     [69 133 136]
     [177 98 134]
     [104 157 106]
     [214 93 14]]}
   :dracula
   {:background-colors
    {:light [68 71 90]
     :dark [40 42 54]}
    :colors
    [[248 248 242]
     [98 114 164]
     [139 233 253]
     [80 250 123]
     [255 184 108]
     [255 121 198]
     [189 147 249]
     [255 85 85]
     [241 250 140]]}})


(defn adjusted-window-size []
  (let [margin 0]
    (map #(- % margin)
         [(.-innerWidth js/window)
          (.-innerHeight js/window)])))


(defn update-window [{:keys [size base-size]}]
  (let [current-size (adjusted-window-size)
        smaller (apply min current-size)]
    {:base-size base-size
     :should-resize (not= size current-size)
     :size current-size
     :object-scale (/ smaller base-size)}))


(defn trunc-last [v len]
  (let [vec-len (count v)]
    (if (< len vec-len)
      (subvec v (- vec-len len) vec-len)
      v)))


(defn lorenz
  ([pos] (lorenz pos {:rho 28 :sigma 10 :beta (/ 8 3)}))
  ([[x y z] {:keys [rho sigma beta]}]
   [(* sigma (- y x))
    (- (* x (- rho z)) y)
    (- (* x y) (* beta z))]))


(defn lorenz-next-point [pos]
  (let [move (map (partial * 0.01) (lorenz pos))]
    (mapv + pos move)))


(defn rescale [{min-val :min max-val :max} val]
  (+ (* (- max-val min-val) val)
     min-val))


(defn init-lines [line-count [x y z]]
  (vec (for [n (range line-count)]
         [[(+ x (/ n line-count 10)) y z]])))


(defn setup []
  (q/frame-rate 60)
  (let [palette-name :gruvbox
        line-count (-> palettes (get palette-name) :colors count)]
    {:palette (palettes palette-name)
     :background :dark
     :show-help false
     :order [0 2]
     :offset-scale [0 0 (/ 1 3)]
     :point-count 10
     :lines (init-lines line-count [1 1 1])
     :window {:size [0 0]
              :base-size 800
              :scale 1
              :should-resize false}}))


(defn draw [state]
  (let [window (:window state)]
    (when (:should-resize window)
      (apply q/resize-sketch (:size window))))
  (q/stroke-weight 10)
  (apply q/background (get-in state [:palette :background-colors (:background state)]))
  (let [order (:order state)
        base-size (get-in state [:window :base-size])
        object-scale (get-in state [:window :object-scale])
        [translation-scale-w translation-scale-h] (mapv (:offset-scale state) order)]
    (when (:show-help state)
      (q/stroke 0 0)
      (q/fill ({:light 0 :dark 255} (:background state)))
      (q/text (str "perspective " order "\n"
                   "trail length " (:point-count state) "\n"
                   "background " (:background state) "\n"
                   "object scale " object-scale "\n"
                   "translation scale " [translation-scale-w translation-scale-h] "\n"
                   "`j` and `k` to adjust trail length\n"
                   "`h` and `l` to switch perspective\n"
                   "`r` and `R` to reset into different initial states\n"
                   "`d` to switch between light and dark backround\n")
              10 10 500 200))


    (q/with-translation [(/ (q/width) 2)
                         (/ (q/height) 2)]

      #_(do
        (q/stroke-weight 3)
        (q/stroke 100)
        (q/fill 0 0)
        (q/line (/ (q/width) 2) 0 (/ (q/width) -2) 0)
        (q/line 0 (/ (q/height) 2) 0 (/ (q/height) -2)))

      (q/with-translation [(* translation-scale-w base-size object-scale -1)
                           (* translation-scale-h base-size object-scale -1)]
        (doseq [[line [r g b]] (map vector (:lines state) (get-in state [:palette :colors]))
                [segnum [p1 p2]] (map vector (range) (partition 2 1 line))
                :let [[x1 y1] (map (mapv #(* % object-scale 10) p1) order)
                      [x2 y2] (map (mapv #(* % object-scale 10) p2) order)]]
          (q/stroke-weight (rescale {:max (* 12 object-scale) :min 1} (/ segnum (dec (count line)))))
          (q/stroke r g b)
          (q/line x1 y1 x2 y2))))))


(defn update-line [line point-count]
  (let [prev-point (last line)]
    (-> line
        (conj (lorenz-next-point prev-point))
        (trunc-last point-count))))


(defn update-state [state]
  (-> state
      (update :lines
              (fn [line]
                (mapv #(update-line % (:point-count state)) line)))
      (update :window update-window)))


(defn on-key-down [state event]
  (case (:key event)
    :? (update state :show-help not)
    :d (update state :background {:light :dark :dark :light})
    :r (assoc state :lines (init-lines (count (:lines state)) [1 1 1]))
    :R (assoc state :lines (init-lines (count (:lines state)) [1 0 0]))
    :h (update-in state [:order 0] #(mod (inc %) 3))
    :l (update-in state [:order 1] #(mod (inc %) 3))
    :k (update state :point-count + 10)
    :j (update state :point-count #(if (< 0 (- % 10)) (- % 10) %))
    state))


(q/defsketch lorenz-attractor-sketch
  :title "Lorenz Attractor"
  :setup setup
  :update update-state
  :draw draw
  :key-pressed on-key-down
  :size [800 800]
  :middleware [m/fun-mode])


(defn init! []
  (println "init!"))
